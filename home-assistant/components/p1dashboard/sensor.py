import asyncio
import logging

import aiohttp
import async_timeout
import voluptuous as vol
from datetime import timedelta

import homeassistant.helpers.config_validation as cv
from homeassistant.components.sensor import PLATFORM_SCHEMA
from homeassistant.const import CONF_API_KEY, CONF_URL, CONF_SCAN_INTERVAL
from homeassistant.helpers.aiohttp_client import async_get_clientsession
from homeassistant.helpers.entity import Entity
from homeassistant.helpers.event import async_track_time_interval

_LOGGER = logging.getLogger(__name__)

DEFAULT_SCAN_INTERVAL = timedelta(minutes=5)

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_URL): cv.string,
    vol.Required(CONF_API_KEY): cv.string,
    vol.Optional(CONF_SCAN_INTERVAL, default=DEFAULT_SCAN_INTERVAL): cv.timedelta
})

SENSOR_INFO = {
    'energy_low': {'name': 'Energy (low)', 'unit': 'kWh', 'icon': 'mdi:flash', 'json_key': 'energyTariff1'},
    'energy_normal': {'name': 'Energy (normal)', 'unit': 'kWh', 'icon': 'mdi:flash', 'json_key': 'energyTariff2'},
    'energy_total': {'name': 'Energy (total)', 'unit': 'kWh', 'icon': 'mdi:flash', 'json_key': 'energyTotal'},
    'gas_total': {'name': 'Gas (total)', 'unit': 'm3', 'icon': 'mdi:fire', 'json_key': 'gasTotal'}
}


async def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    devices = [P1Sensor(hass, key) for key in SENSOR_INFO]
    async_add_entities(devices)

    data = P1Data(hass, devices, config)
    await data.update()


class P1Sensor(Entity):
    def __init__(self, hass, key):
        self.hass = hass
        self.entity_id = 'p1dashboard.{}'.format(key)

        self._state = None
        self._key = key

    @property
    def name(self):
        return SENSOR_INFO[self._key]['name']

    @property
    def state(self):
        return self._state

    @property
    def unit_of_measurement(self):
        return SENSOR_INFO[self._key]['unit']

    @property
    def icon(self):
        return SENSOR_INFO[self._key]['icon']

    def reflect_api(self, data):
        json_key = SENSOR_INFO[self._key]['json_key']
        new_state = data[json_key]
        has_change = new_state != self._state

        self._state = new_state
        return has_change


class P1Data:
    def __init__(self, hass, devices, config):
        self._hass = hass
        self._devices = devices
        self._url = config.get(CONF_URL)
        self._api_key = config.get(CONF_API_KEY)
        self._scan_interval = config.get(CONF_SCAN_INTERVAL)

        async_track_time_interval(hass, self.update, self._scan_interval)

    async def update(self, *_):
        try:
            http_session = async_get_clientsession(self._hass)

            with async_timeout.timeout(10, loop=self._hass.loop):
                response = await http_session.get(self._url, headers={'Authorization': 'Bearer %s' % self._api_key})

            if response.status != 200:
                _LOGGER.error("API request resulted in status code %d" % response.status)
                return
        except (asyncio.TimeoutError, aiohttp.ClientError) as error:
            _LOGGER.error("API request resulted in exception %s" % error)
            return

        data = await response.json()
        await self.update_state(data)

        _LOGGER.info("Successfully updated sensor data")

    async def update_state(self, data):
        tasks = []

        for device in self._devices:
            if device.reflect_api(data):
                tasks.append(device.async_update_ha_state())

        if tasks:
            await asyncio.wait(tasks, loop=self._hass.loop)
