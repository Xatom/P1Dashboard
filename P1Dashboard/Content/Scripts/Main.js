﻿const Main = () => {
    const connection = new signalR.HubConnectionBuilder()
        .withUrl("/Hubs/P1")
        .build();

    const p1Ui = new Vue({
        el: "#p1-ui",
        data: {
            connected: false,
            info: null
        },
        methods: {

        },
        computed: {
            hasInfo() {
                return this.info !== null;
            },
            hasMessageLong() {
                return this.info.messageLong !== "";
            },
            currentUsage() {
                return Helpers.safeSubstract(this.info.powerDelivered.live, this.info.powerReturned.live);
            }
        }
    });

    connection.on("Info", info => {
        p1Ui.info = info;
    });

    connection.onclose(() => {
        p1Ui.connected = false;
    });

    connection.start().then(() => {
        p1Ui.connected = true;
    });
};
