const Helpers = {
    // All P1 values have 3 decimal places of precision.
    precision: 3,

    safeSubstract: (a, b) => {
        return (a - b).toFixed(Helpers.precision);
    }
};
