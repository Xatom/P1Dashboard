const Charts = (model) => {
    const chartsUi = new Vue({
        el: "#charts-ui",
        data: {
            charts: {}
        },
        methods: {
            hasChart(prefix) {
                return this.charts[prefix] !== null;
            },
            toggleLegend(prefix) {
                const charts = this.charts[prefix];

                toggleLegend(charts.energy);
                toggleLegend(charts.gas);
            }
        },
        computed: {}
    });

    chartsUi.charts = {
        day: createChart("day", model.DayMeasurements),
        week: createChart("week", model.WeekMeasurements),
        month: createChart("month", model.MonthMeasurements),
        year: createChart("year", model.YearMeasurements)
    };

    function createChart(idPrefix, data) {
        if (data.length <= 2)
            return null;

        const energyCtx = document.getElementById(idPrefix + "-chart-energy").getContext("2d"),
            gasCtx = document.getElementById(idPrefix + "-chart-gas").getContext("2d"),
            labels = [],
            energyDeliveredTotalDataset = createDataset("Delivered (Total)", "#B71C1C", "circle", false),
            energyDeliveredTariff1Dataset = createDataset("Delivered (Tariff 1)", "#E53935", "cross", true),
            energyDeliveredTariff2Dataset = createDataset("Delivered (Tariff 2)", "#E57373", "rect", true),
            energyReturnedTotalDataset = createDataset("Returned (Total)", "#1B5E20", "circle", false),
            energyReturnedTariff1Dataset = createDataset("Returned (Tariff 1)", "#43A047", "cross", true),
            energyReturnedTariff2Dataset = createDataset("Returned (Tariff 2)", "#81C784", "rect", true),
            gasDeliveredDataset = createDataset("Delivered", "#F57F17", "circle", false);

        for (let i = 0; i < data.length - 1; i++) {
            const cData = data[i], nData = data[i + 1];
            labels.push(data[i].Date);

            // The measurements are just a snapshot at the start of each period.
            // To calculate the actual usage of a period, simply substract the next period's snapshot from the current.
            // This will result in the last period to be ignored, because it is incomplete.
            addDifference(energyDeliveredTotalDataset, nData, cData, pData => pData.EnergyDeliveredTariff1 + pData.EnergyDeliveredTariff2);
            addDifference(energyDeliveredTariff1Dataset, nData, cData, pData => pData.EnergyDeliveredTariff1);
            addDifference(energyDeliveredTariff2Dataset, nData, cData, pData => pData.EnergyDeliveredTariff2);
            addDifference(energyReturnedTotalDataset, nData, cData, pData => pData.EnergyReturnedTariff1 + pData.EnergyReturnedTariff2);
            addDifference(energyReturnedTariff1Dataset, nData, cData, pData => pData.EnergyReturnedTariff1);
            addDifference(energyReturnedTariff2Dataset, nData, cData, pData => pData.EnergyReturnedTariff2);
            addDifference(gasDeliveredDataset, nData, cData, pData => pData.GasDelivered);
        }

        return {
            energy: renderChart(energyCtx, labels, [
                energyDeliveredTotalDataset,
                energyDeliveredTariff1Dataset,
                energyDeliveredTariff2Dataset,
                energyReturnedTotalDataset,
                energyReturnedTariff1Dataset,
                energyReturnedTariff2Dataset
            ], "kWh"),
            gas: renderChart(gasCtx, labels, [
                gasDeliveredDataset
            ], "m3")
        };
    }

    function createDataset(label, color, style, hidden) {
        return {
            label: label,
            borderColor: color,
            backgroundColor: color,
            pointStyle: style,
            fill: false,
            hidden: hidden,
            data: []
        };
    }

    function addDifference(dataset, a, b, selector) {
        const value = Helpers.safeSubstract(selector(a), selector(b));
        dataset.data.push(value);
    }

    function renderChart(ctx, labels, datasets, ylabel) {
        return new Chart(ctx, {
            type: "line",
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: ylabel
                        }
                    }]
                }
            }
        })
    }

    function toggleLegend(chart) {
        chart.options.legend.display = !chart.options.legend.display;
        chart.update();
    }
};
