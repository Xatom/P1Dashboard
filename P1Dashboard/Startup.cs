﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using P1Dashboard.Data;
using P1Dashboard.Extensions;
using P1Dashboard.Hubs;
using P1Dashboard.Logging;
using P1Dashboard.P1;
using P1Dashboard.P1.Handlers;

namespace P1Dashboard
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public void ConfigureServices(IServiceCollection services)
        {
            var settings = _configuration.Get<Settings>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Home/Login";
                    options.LogoutPath = "/Home/Logout";
                });

            services.AddControllersWithViews()
                .AddNewtonsoftJson();
            services.AddSignalR();
            services.AddDbContext<DatabaseContext>(d => d.UseSqlite(settings.ConnectionString));

            services.AddSingleton(settings);
            services.AddSingleton<IHostedService, P1Reader>();
            services.AddSingleton<WebLoggerProvider>();

            services.AddSingleton<CurrentInfoHandler>();
            services.AddSingleton<IInfoHandler>(s => s.GetService<CurrentInfoHandler>());
            services.AddSingleton<StatisticsInfoHandler>();
            services.AddSingleton<IInfoHandler>(s => s.GetService<StatisticsInfoHandler>());
        }

        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureLoggers(app);
            PrintVersion(app);
            MigrateDatabase(app);
            UseForwardedHeaders(app);

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/Error/500");

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    // Cache all static files for four weeks, we'll append a version to all URLs.
                    context.Context.Response.Headers.Append("Cache-Control", "public,max-age=2419200");
                }
            });

            app.UseStatusCodePagesWithReExecute("/Error/{0}");

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<P1Hub>("/Hubs/P1");
                endpoints.MapDefaultControllerRoute();
            });
        }

        private static void ConfigureLoggers(IApplicationBuilder app)
        {
            var settings = app.ApplicationServices.GetService<Settings>();
            var webLoggerProvider = app.ApplicationServices.GetService<WebLoggerProvider>();
            var loggerFactory = app.ApplicationServices.GetService<ILoggerFactory>();
            
            loggerFactory.AddProvider(webLoggerProvider);

            if (string.IsNullOrEmpty(settings.SentryDsn))
                return;

            loggerFactory.AddSentry(s =>
            {
                s.Dsn = settings.SentryDsn;
                s.Release = settings.Version;
            });
        }

        private static void PrintVersion(IApplicationBuilder app)
        {
            var settings = app.ApplicationServices.GetService<Settings>();
            var logger = app.ApplicationServices.GetService<ILogger<Startup>>();

            logger.LogInformation($"Dashboard @ {settings.Version}");
        }

        private static void MigrateDatabase(IApplicationBuilder app)
        {
            var settings = app.ApplicationServices.GetService<Settings>();
            var dbContextOptions = DatabaseContext.GetOptions(settings);

            using (var database = new DatabaseContext(dbContextOptions))
                database.Database.Migrate();
        }

        private static void UseForwardedHeaders(IApplicationBuilder app)
        {
            var settings = app.ApplicationServices.GetService<Settings>();

            if (string.IsNullOrEmpty(settings.ProxyServer))
                return;

            ISet<IPAddress> proxyIpAddresses;
            var logger = app.ApplicationServices.GetService<ILogger<Startup>>();

            try
            {
                proxyIpAddresses = Dns.GetHostAddresses(settings.ProxyServer).ToIPv4And6();
                logger.LogInformation($"Using proxy server IPs: {proxyIpAddresses.ToLogMessage()}");
            }
            catch (Exception exception)
            {
                logger.LogError(exception, "Failed to get proxy server IPs");
                return;
            }

            var options = new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            };

            foreach (var proxyIpAddress in proxyIpAddresses)
                options.KnownProxies.Add(proxyIpAddress);

            app.UseForwardedHeaders(options);
        }
    }
}
