﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace P1Dashboard.P1
{
    public static class P1Parser
    {
        private static readonly CultureInfo Culture = CultureInfo.InvariantCulture;
        private static readonly Regex CodeRegex = new Regex(@"^(\d+-\d+:\d+\.\d+\.\d+)", RegexOptions.Compiled);
        private static readonly Regex ValueRegex = new Regex(@"\((.*?)\)", RegexOptions.Compiled);

        private static readonly Dictionary<string, Action<P1Info, MatchCollection>> ParserByCode =
            new Dictionary<string, Action<P1Info, MatchCollection>>
        {
            ["1-3:0.2.8"] = ParseVersion,
            ["0-0:1.0.0"] = ParseTimestamp,
            ["0-0:96.1.1"] = ParseEquipmentId,
            ["1-0:1.8.1"] = ParseEnergyDeliveredTariff1,
            ["1-0:1.8.2"] = ParseEnergyDeliveredTariff2,
            ["1-0:2.8.1"] = ParseEnergyReturnedTariff1,
            ["1-0:2.8.2"] = ParseEnergyReturnedTariff2,
            ["0-0:96.14.0"] = ParseElectricityTariff,
            ["1-0:1.7.0"] = ParsePowerDelivered,
            ["1-0:2.7.0"] = ParsePowerReturned,
            ["0-0:96.7.21"] = ParseElectricityFailures,
            ["0-0:96.7.9"] = ParseElectricityLongFailures,
            ["1-0:99.97.0"] = ParseElectricityFailureLog,
            ["1-0:32.32.0"] = ParseVoltageSagsL1,
            ["1-0:52.32.0"] = ParseVoltageSagsL2,
            ["1-0:72.32.0"] = ParseVoltageSagsL3,
            ["1-0:32.36.0"] = ParseVoltageSwellsL1,
            ["1-0:52.36.0"] = ParseVoltageSwellsL2,
            ["1-0:72.36.0"] = ParseVoltageSwellsL3,
            ["0-0:96.13.0"] = ParseMessageLong,
            ["1-0:32.7.0"] = ParseVoltageL1,
            ["1-0:52.7.0"] = ParseVoltageL2,
            ["1-0:72.7.0"] = ParseVoltageL3,
            ["1-0:31.7.0"] = ParseCurrentL1,
            ["1-0:51.7.0"] = ParseCurrentL2,
            ["1-0:71.7.0"] = ParseCurrentL3,
            ["1-0:21.7.0"] = ParsePowerDeliveredL1,
            ["1-0:41.7.0"] = ParsePowerDeliveredL2,
            ["1-0:61.7.0"] = ParsePowerDeliveredL3,
            ["1-0:22.7.0"] = ParsePowerReturnedL1,
            ["1-0:42.7.0"] = ParsePowerReturnedL2,
            ["1-0:62.7.0"] = ParsePowerReturnedL3,
            ["0-1:24.1.0"] = ParseGasDeviceType,
            ["0-1:96.1.0"] = ParseGasEquipmentId,
            ["0-1:24.2.1"] = ParseGasDelivered
        };

        public static P1Info Parse(IEnumerable<string> lines)
        {
            var info = new P1Info();

            foreach (var line in lines)
            {
                var code = CodeRegex.Match(line);

                if (!ParserByCode.TryGetValue(code.Groups[1].Value, out var parser))
                    continue;

                var values = ValueRegex.Matches(line);

                try
                {
                    parser(info, values);
                }
                catch (Exception exception)
                {
                    exception.Data.Add("Line", line);
                    throw;
                }
            }

            return info;
        }

        private static void ParseVersion(P1Info info, MatchCollection matches) =>
            info.Version = matches[0].Groups[1].Value;

        private static void ParseTimestamp(P1Info info, MatchCollection matches) =>
            info.Timestamp = ParseTimestamp(matches[0].Groups[1].Value);

        private static void ParseEquipmentId(P1Info info, MatchCollection matches) =>
            info.EquipmentId = matches[0].Groups[1].Value;

        private static void ParseEnergyDeliveredTariff1(P1Info info, MatchCollection matches) =>
            info.EnergyDelivered.Tariff1 = ParseKwh(matches[0].Groups[1].Value);

        private static void ParseEnergyDeliveredTariff2(P1Info info, MatchCollection matches) =>
            info.EnergyDelivered.Tariff2 = ParseKwh(matches[0].Groups[1].Value);

        private static void ParseEnergyReturnedTariff1(P1Info info, MatchCollection matches) =>
            info.EnergyReturned.Tariff1 = ParseKwh(matches[0].Groups[1].Value);

        private static void ParseEnergyReturnedTariff2(P1Info info, MatchCollection matches) =>
            info.EnergyReturned.Tariff2 = ParseKwh(matches[0].Groups[1].Value);

        private static void ParseElectricityTariff(P1Info info, MatchCollection matches) =>
            info.ElectricityTariff = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParsePowerDelivered(P1Info info, MatchCollection matches) =>
            info.PowerDelivered.Live = ParseKw(matches[0].Groups[1].Value);

        private static void ParsePowerReturned(P1Info info, MatchCollection matches) =>
            info.PowerReturned.Live = ParseKw(matches[0].Groups[1].Value);

        private static void ParseElectricityFailures(P1Info info, MatchCollection matches) =>
            info.ElectricityFailures = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseElectricityLongFailures(P1Info info, MatchCollection matches) =>
            info.ElectricityLongFailures = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseElectricityFailureLog(P1Info info, MatchCollection matches) =>
            info.ElectricityFailureLog = string.Join(" / ", matches.Select(m => m.Groups[1].Value));

        private static void ParseVoltageSagsL1(P1Info info, MatchCollection matches) =>
            info.VoltageSags.L1 = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseVoltageSagsL2(P1Info info, MatchCollection matches) =>
            info.VoltageSags.L2 = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseVoltageSagsL3(P1Info info, MatchCollection matches) =>
            info.VoltageSags.L3 = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseVoltageSwellsL1(P1Info info, MatchCollection matches) =>
            info.VoltageSwells.L1 = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseVoltageSwellsL2(P1Info info, MatchCollection matches) =>
            info.VoltageSwells.L2 = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseVoltageSwellsL3(P1Info info, MatchCollection matches) =>
            info.VoltageSwells.L3 = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseMessageLong(P1Info info, MatchCollection matches) =>
            info.MessageLong = matches[0].Groups[1].Value;

        private static void ParseVoltageL1(P1Info info, MatchCollection matches) =>
            info.Voltage.L1 = ParseV(matches[0].Groups[1].Value);

        private static void ParseVoltageL2(P1Info info, MatchCollection matches) =>
            info.Voltage.L2 = ParseV(matches[0].Groups[1].Value);

        private static void ParseVoltageL3(P1Info info, MatchCollection matches) =>
            info.Voltage.L3 = ParseV(matches[0].Groups[1].Value);

        private static void ParseCurrentL1(P1Info info, MatchCollection matches) =>
            info.Current.L1 = ParseA(matches[0].Groups[1].Value);

        private static void ParseCurrentL2(P1Info info, MatchCollection matches) =>
            info.Current.L2 = ParseA(matches[0].Groups[1].Value);

        private static void ParseCurrentL3(P1Info info, MatchCollection matches) =>
            info.Current.L3 = ParseA(matches[0].Groups[1].Value);

        private static void ParsePowerDeliveredL1(P1Info info, MatchCollection matches) =>
            info.PowerDelivered.L1 = ParseKw(matches[0].Groups[1].Value);
        
        private static void ParsePowerDeliveredL2(P1Info info, MatchCollection matches) =>
            info.PowerDelivered.L2 = ParseKw(matches[0].Groups[1].Value);

        private static void ParsePowerDeliveredL3(P1Info info, MatchCollection matches) =>
            info.PowerDelivered.L3 = ParseKw(matches[0].Groups[1].Value);

        private static void ParsePowerReturnedL1(P1Info info, MatchCollection matches) =>
            info.PowerReturned.L1 = ParseKw(matches[0].Groups[1].Value);

        private static void ParsePowerReturnedL2(P1Info info, MatchCollection matches) =>
            info.PowerReturned.L2 = ParseKw(matches[0].Groups[1].Value);

        private static void ParsePowerReturnedL3(P1Info info, MatchCollection matches) =>
            info.PowerReturned.L3 = ParseKw(matches[0].Groups[1].Value);

        private static void ParseGasDeviceType(P1Info info, MatchCollection matches) =>
            info.GasDeviceType = Convert.ToInt32(matches[0].Groups[1].Value);

        private static void ParseGasEquipmentId(P1Info info, MatchCollection matches) =>
            info.GasEquipmentId = matches[0].Groups[1].Value;

        private static void ParseGasDelivered(P1Info info, MatchCollection matches)
        {
            info.GasTimestamp = ParseTimestamp(matches[0].Groups[1].Value);
            info.GasDelivered = ParseM3(matches[1].Groups[1].Value);
        }

        private static DateTime ParseTimestamp(string value) =>
            // Strip the last character which is either S (summer) or W (winter) time.
            DateTime.ParseExact(value.Substring(0, value.Length - 1), "yyMMddHHmmss", Culture);

        private static decimal ParseKwh(string value) =>
            // Strip the last 4 characters (*kWh).
            Convert.ToDecimal(value.Substring(0, value.Length - 4), Culture);

        private static decimal ParseKw(string value) =>
            // Strip the last 3 characters (*kW).
            Convert.ToDecimal(value.Substring(0, value.Length - 3), Culture);

        private static decimal ParseV(string value) =>
            // Strip the last 2 characters (*V).
            Convert.ToDecimal(value.Substring(0, value.Length - 2), Culture);

        private static decimal ParseA(string value) =>
            // Strip the last 2 characters (*A).
            Convert.ToDecimal(value.Substring(0, value.Length - 2), Culture);

        private static decimal ParseM3(string value) =>
            // Strip the last 3 characters (*m3).
            Convert.ToDecimal(value.Substring(0, value.Length - 3), Culture);
    }
}
