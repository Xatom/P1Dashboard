﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace P1Dashboard.P1
{
    public sealed class P1Info
    {
        public sealed class LInts
        {
            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public int L1 { get; set; }

            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public int L2 { get; set; }

            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public int L3 { get; set; }
        }

        public class LDecimals
        {
            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public decimal L1 { get; set; }

            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public decimal L2 { get; set; }

            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public decimal L3 { get; set; }
        }

        public sealed class TariffDecimals
        {
            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public decimal Tariff1 { get; set; }

            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public decimal Tariff2 { get; set; }
        }

        public sealed class PowerInfo : LDecimals
        {
            [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
            public decimal Live { get; set; }
        }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string Version { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public DateTime Timestamp { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string EquipmentId { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public TariffDecimals EnergyDelivered { get; } = new TariffDecimals();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public TariffDecimals EnergyReturned { get; } = new TariffDecimals();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int ElectricityTariff { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public PowerInfo PowerDelivered { get; } = new PowerInfo();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public PowerInfo PowerReturned { get; } = new PowerInfo();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int ElectricityFailures { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int ElectricityLongFailures { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string ElectricityFailureLog { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public LInts VoltageSags { get; } = new LInts();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public LInts VoltageSwells { get; } = new LInts();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string MessageLong { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public LDecimals Voltage { get; } = new LDecimals();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public LDecimals Current { get; } = new LDecimals();

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int GasDeviceType { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string GasEquipmentId { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public DateTime GasTimestamp { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public decimal GasDelivered { get; set; }
    }
}
