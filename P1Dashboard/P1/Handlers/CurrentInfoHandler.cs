﻿using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace P1Dashboard.P1.Handlers
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class CurrentInfoHandler : IInfoHandler
    {
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();

        private P1Info _info;

        public P1Info Info
        {
            get
            {
                _lock.EnterReadLock();

                try
                {
                    return _info;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public Task HandleMeasurement(P1Info info)
        {
            _lock.EnterWriteLock();

            _info = info;

            _lock.ExitWriteLock();

            return Task.CompletedTask;
        }
    }
}
