﻿using System.Threading.Tasks;

namespace P1Dashboard.P1.Handlers
{
    public interface IInfoHandler
    {
        Task HandleMeasurement(P1Info info);
    }
}
