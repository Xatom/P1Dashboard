﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using P1Dashboard.Data;
using P1Dashboard.Models;

namespace P1Dashboard.P1.Handlers
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class StatisticsInfoHandler : IInfoHandler
    {
        public StatisticsInfoHandler(Settings settings)
        {
            _dbContextOptions = DatabaseContext.GetOptions(settings);
            _firstDayOfWeek = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

            using (var database = new DatabaseContext(_dbContextOptions))
            {
                _latestMeasurements = new MeasurementsModel
                {
                    Day = database.OrderedMeasurements.LastOrDefault(m => m.Period == MeasurementPeriod.Day),
                    Week = database.OrderedMeasurements.LastOrDefault(m => m.Period == MeasurementPeriod.Week),
                    Month = database.OrderedMeasurements.LastOrDefault(m => m.Period == MeasurementPeriod.Month),
                    Year = database.OrderedMeasurements.LastOrDefault(m => m.Period == MeasurementPeriod.Year)
                };
            }
        }

        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        private readonly DbContextOptions _dbContextOptions;
        private readonly DayOfWeek _firstDayOfWeek;

        private MeasurementsModel _latestMeasurements;

        public MeasurementsModel LatestMeasurements
        {
            get
            {
                _lock.EnterReadLock();

                try
                {
                    return _latestMeasurements;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public async Task HandleMeasurement(P1Info info)
        {
            _lock.EnterWriteLock();

            try
            {
                await UpdateStatistics(info);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        private async Task UpdateStatistics(P1Info info)
        {
            var date = info.Timestamp.Date;
            // Create a new instance to prevent altering the current instance outside this class.
            var latestMeasurements = new MeasurementsModel();

            using (var database = new DatabaseContext(_dbContextOptions))
            {
                latestMeasurements.Day = IsLaterDay(date)
                    ? AddMeasurement(database, info, MeasurementPeriod.Day)
                    : _latestMeasurements.Day;

                latestMeasurements.Week = IsLaterWeek(date)
                    ? AddMeasurement(database, info, MeasurementPeriod.Week)
                    : _latestMeasurements.Week;

                latestMeasurements.Month = IsLaterMonth(date)
                    ? AddMeasurement(database, info, MeasurementPeriod.Month)
                    : _latestMeasurements.Month;

                latestMeasurements.Year = IsLaterYear(date)
                    ? AddMeasurement(database, info, MeasurementPeriod.Year)
                    : _latestMeasurements.Year;

                await database.SaveChangesAsync();
            }

            _latestMeasurements = latestMeasurements;
        }

        private bool IsLaterDay(DateTime date) =>
            _latestMeasurements.Day == null || date > _latestMeasurements.Day.Date;

        private bool IsLaterWeek(DateTime date)
        {
            var days = _firstDayOfWeek - date.DayOfWeek;
            if (days > 0) days -= 7;

            var weekStart = date.AddDays(days);

            return _latestMeasurements.Week == null || weekStart > _latestMeasurements.Week.Date;
        }

        private bool IsLaterMonth(DateTime date)
        {
            var monthStart = new DateTime(date.Year, date.Month, 1);

            return _latestMeasurements.Month == null || monthStart > _latestMeasurements.Month.Date;
        }

        private bool IsLaterYear(DateTime date)
        {
            var yearStart = new DateTime(date.Year, 1, 1);

            return _latestMeasurements.Year == null || yearStart > _latestMeasurements.Year.Date;
        }

        private static MeasurementModel AddMeasurement(DatabaseContext database, P1Info info, MeasurementPeriod period)
        {
            var measurement = MeasurementModel.FromP1Info(info, period);
            database.Measurements.Add(measurement);

            return measurement;
        }
    }
}
