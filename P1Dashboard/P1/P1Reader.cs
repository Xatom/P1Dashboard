﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using P1Dashboard.Hubs;
using P1Dashboard.P1.Handlers;

namespace P1Dashboard.P1
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class P1Reader : BackgroundService
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        public P1Reader(ILogger<P1Reader> logger, Settings settings, IEnumerable<IInfoHandler> infoHandlers, IHubContext<P1Hub> p1Hub)
        {
            _logger = logger;
            _settings = settings;
            _infoHandlers = infoHandlers;
            _p1Hub = p1Hub;

            // Make sure to process the first incoming message.
            _infoCount = settings.UpdateInterval;
        }

        private readonly ILogger _logger;
        private readonly Settings _settings;
        private readonly IEnumerable<IInfoHandler> _infoHandlers;
        private readonly IHubContext<P1Hub> _p1Hub;

        private int _infoCount;

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    using (var tcpClient = new TcpClient())
                        await ProcessData(tcpClient, cancellationToken);
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, "Failed to read from server, retrying.");
                }

                await Task.Delay(_settings.ReconnectInterval, cancellationToken);
            }
        }

        private async Task ProcessData(TcpClient tcpClient, CancellationToken cancellationToken = default)
        {
            await tcpClient.ConnectAsync(_settings.ServerHost, _settings.ServerPort);
            var reader = new StreamReader(tcpClient.GetStream(), Encoding.ASCII);

            var hasStart = false;
            var lines = new List<string>();
            var line = await reader.ReadLineAsync();

            do
            {
                if (!hasStart)
                {
                    if (line == string.Empty)
                        hasStart = true;
                }
                else if (line.StartsWith('!'))
                {
                    await SendInfo(lines);
                    hasStart = false;
                    lines.Clear();
                }
                else
                {
                    lines.Add(line);
                }
            } while (!cancellationToken.IsCancellationRequested && (line = await reader.ReadLineAsync()) != null);
        }

        private async Task SendInfo(IEnumerable<string> lines)
        {
            if (++_infoCount < _settings.UpdateInterval)
                return;

            _infoCount = 0;

            try
            {
                var p1Info = P1Parser.Parse(lines);

                await _p1Hub.Clients.All.SendAsync(P1Hub.InfoCommand, p1Info);

                foreach (var infoHandler in _infoHandlers)
                    await infoHandler.HandleMeasurement(p1Info);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Failed to parse P1 info, skipping.");
            }
        }
    }
}
