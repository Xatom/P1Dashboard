﻿namespace P1Dashboard.Data
{
    public enum MeasurementPeriod
    {
        Day,
        Week,
        Month,
        Year
    }
}
