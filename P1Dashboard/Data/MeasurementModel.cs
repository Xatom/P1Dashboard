﻿using System;
using System.Diagnostics.CodeAnalysis;
using P1Dashboard.P1;

namespace P1Dashboard.Data
{
    public class MeasurementModel
    {
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public int Id { get; set; }

        public decimal EnergyDeliveredTariff1 { get; set; }

        public decimal EnergyDeliveredTariff2 { get; set; }

        public decimal EnergyReturnedTariff1 { get; set; }

        public decimal EnergyReturnedTariff2 { get; set; }

        public decimal GasDelivered { get; set; }

        public MeasurementPeriod Period { get; set; }

        public DateTime Date { get; set; }

        public static MeasurementModel FromP1Info(P1Info info, MeasurementPeriod period) =>
            new MeasurementModel
            {
                EnergyDeliveredTariff1 = info.EnergyDelivered.Tariff1,
                EnergyDeliveredTariff2 = info.EnergyDelivered.Tariff2,
                EnergyReturnedTariff1 = info.EnergyReturned.Tariff1,
                EnergyReturnedTariff2 = info.EnergyReturned.Tariff2,
                GasDelivered = info.GasDelivered,
                Period = period,
                Date = info.Timestamp.Date
            };
    }
}
