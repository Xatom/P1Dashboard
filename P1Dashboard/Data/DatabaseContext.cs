﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using P1Dashboard.Extensions;

namespace P1Dashboard.Data
{
    public class DatabaseContext : DbContext, IDesignTimeDbContextFactory<DatabaseContext>
    {
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public DbSet<MeasurementModel> Measurements { get; set; }

        public IQueryable<MeasurementModel> OrderedMeasurements =>
            Measurements.Ordered();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MeasurementModel>()
                .HasIndex(m => m.Period);

            modelBuilder.Entity<MeasurementModel>()
                .HasIndex(m => new { m.Period, m.Date });
        }

        public static DbContextOptions<DatabaseContext> GetOptions(Settings settings)
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            dbContextOptionsBuilder.UseSqlite(settings.ConnectionString);

            return dbContextOptionsBuilder.Options;
        }

        public DatabaseContext CreateDbContext(string[] args)
        {
            var settings = new Settings();

            return new DatabaseContext(GetOptions(settings));
        }
    }
}
