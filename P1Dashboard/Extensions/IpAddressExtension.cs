﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace P1Dashboard.Extensions
{
    public static class IpAddressExtension
    {
        public static ISet<IPAddress> ToIPv4And6(this IEnumerable<IPAddress> ipAddresses)
        {
            var mappedIpAddresses = new HashSet<IPAddress>();

            foreach (var ipAddress in ipAddresses)
            {
                mappedIpAddresses.Add(ipAddress.MapToIPv6());

                if (ipAddress.IsIPv4MappedToIPv6)
                    mappedIpAddresses.Add(ipAddress.MapToIPv4());
                else if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                    mappedIpAddresses.Add(ipAddress);
            }

            return mappedIpAddresses;
        }

        public static string ToLogMessage(this IEnumerable<IPAddress> ipAddresses) =>
            string.Join(", ", ipAddresses);
    }
}
