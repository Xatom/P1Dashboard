﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using P1Dashboard.Data;

namespace P1Dashboard.Extensions
{
    public static class MeasurementsExtension
    {
        public static IQueryable<MeasurementModel> Ordered(this IQueryable<MeasurementModel> measurements) =>
            measurements.OrderBy(m => m.Id);

        public static IQueryable<MeasurementModel> ByPeriod(this IQueryable<MeasurementModel> measurements, MeasurementPeriod period) =>
            measurements.Where(m => m.Period == period);

        public static async Task<List<MeasurementModel>> LimitAsync(this IQueryable<MeasurementModel> query, int? limit)
        {
            if (!limit.HasValue)
                return await query.ToListAsync();

            var count = await query.CountAsync();
            var skip = count > limit.Value ? count - limit.Value : 0;
            return await query.Skip(skip).Take(limit.Value).ToListAsync();
        }
    }
}
