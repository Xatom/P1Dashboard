﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace P1Dashboard.Controllers
{
    [AllowAnonymous]
    public sealed class ErrorController : BaseController
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        public ErrorController(ILogger<ErrorController> logger) : base(logger)
        {
        }

        [ActionName("404")]
        public IActionResult Get404()
        {
            return View();
        }

        [ActionName("500")]
        public IActionResult Get500()
        {
            return View();
        }
    }
}
