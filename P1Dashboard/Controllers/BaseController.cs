﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace P1Dashboard.Controllers
{
    [Authorize]
    public abstract class BaseController : Controller
    {
        protected BaseController(ILogger logger)
        {
            _logger = logger;
        }

        private readonly ILogger _logger;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            _logger.LogInformation($"Request from {HttpContext.Connection.RemoteIpAddress} ({HttpContext.Connection.RemotePort}) via {Request.Host} ({Request.Scheme})!");
        }
    }
}
