﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using P1Dashboard.Data;
using P1Dashboard.Extensions;
using P1Dashboard.Models;
using P1Dashboard.P1.Handlers;

namespace P1Dashboard.Controllers
{
    public class StatisticsController : BaseController
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        public StatisticsController(ILogger<StatisticsController> logger, Settings settings, DatabaseContext database, CurrentInfoHandler currentInfo) : base(logger)
        {
            _settings = settings;
            _database = database;
            _currentInfo = currentInfo;
        }

        private readonly Settings _settings;
        private readonly DatabaseContext _database;
        private readonly CurrentInfoHandler _currentInfo;

        [HttpGet]
        [ActionName("Index")]
        public IActionResult GetIndex()
        {
            return View();
        }

        [HttpGet]
        [ActionName("Charts")]
        public async Task<IActionResult> GetCharts()
        {
            var current = _currentInfo.Info;
            const int limit = 13; // Effectively 12 entries because the last one is just used for calculations.

            var model = new ChartsModel
            {
                TimeStamp = current.Timestamp,
                DayMeasurements = await _database.OrderedMeasurements.ByPeriod(MeasurementPeriod.Day).LimitAsync(limit),
                WeekMeasurements = await _database.OrderedMeasurements.ByPeriod(MeasurementPeriod.Week).LimitAsync(limit),
                MonthMeasurements = await _database.OrderedMeasurements.ByPeriod(MeasurementPeriod.Month).LimitAsync(limit),
                YearMeasurements = await _database.OrderedMeasurements.ByPeriod(MeasurementPeriod.Year).LimitAsync(limit)
            };

            model.DayMeasurements.Add(MeasurementModel.FromP1Info(current, MeasurementPeriod.Day));
            model.WeekMeasurements.Add(MeasurementModel.FromP1Info(current, MeasurementPeriod.Week));
            model.MonthMeasurements.Add(MeasurementModel.FromP1Info(current, MeasurementPeriod.Month));
            model.YearMeasurements.Add(MeasurementModel.FromP1Info(current, MeasurementPeriod.Year));

            return View(model);
        }
    }
}
