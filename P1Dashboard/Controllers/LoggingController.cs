﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace P1Dashboard.Controllers
{
    public class LoggingController : BaseController
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        public LoggingController(ILogger<LoggingController> logger) : base(logger)
        {
        }

        [HttpGet]
        [ActionName("Index")]
        public IActionResult GetIndex()
        {
            return View();
        }
    }
}
