﻿using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using P1Dashboard.Models;

namespace P1Dashboard.Controllers
{
    public sealed class HomeController : BaseController
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        public HomeController(ILogger<HomeController> logger, Settings settings) : base(logger)
        {
            _settings = settings;
        }

        private readonly Settings _settings;

        [HttpGet]
        [ActionName("Index")]
        public IActionResult GetIndex()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [ActionName("Login")]
        public IActionResult GetLogin(string returnUrl)
        {
            return View(new LoginModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("Login")]
        public async Task<IActionResult> PostLoginAsync(LoginModel model)
        {
            if (_settings.Username != model.Username || _settings.Password != model.Password)
                ModelState.AddModelError(nameof(model.Username), "Invalid username and/or password");

            if (!ModelState.IsValid)
                return View(model);

            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, _settings.Username),
            }, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(identity), new AuthenticationProperties
                {
                    IsPersistent = model.RememberMe
                });

            if (Url.IsLocalUrl(model.ReturnUrl))
                return Redirect(model.ReturnUrl);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [ActionName("Logout")]
        public async Task<IActionResult> GetLogoutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login");
        }
    }
}
