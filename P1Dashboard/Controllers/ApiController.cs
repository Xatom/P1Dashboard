﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using P1Dashboard.Attributes;
using P1Dashboard.Data;
using P1Dashboard.Extensions;
using P1Dashboard.P1.Handlers;

namespace P1Dashboard.Controllers
{
    [Produces("application/json")]
    [AllowAnonymous]
    [ApiController]
    [ApiKey]
    public class ApiController : BaseController
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        public ApiController(ILogger<ApiController> logger, CurrentInfoHandler currentInfoHandler, DatabaseContext database) : base(logger)
        {
            _currentInfoHandler = currentInfoHandler;
            _database = database;
        }

        private readonly CurrentInfoHandler _currentInfoHandler;
        private readonly DatabaseContext _database;

        [HttpGet]
        [Route("api/usage/{period}")]
        public async Task<dynamic> GetEnergy(MeasurementPeriod period)
        {
            var current = _currentInfoHandler.Info;
            var measurement = await _database.OrderedMeasurements.ByPeriod(period).LastAsync();

            var energyTariff1 = current.EnergyDelivered.Tariff1 - measurement.EnergyDeliveredTariff1;
            var energyTariff2 = current.EnergyDelivered.Tariff2 - measurement.EnergyDeliveredTariff2;

            return new
            {
                EnergyTariff1 = energyTariff1,
                EnergyTariff2 = energyTariff2,
                EnergyTotal = energyTariff1 + energyTariff2,
                GasTotal = current.GasDelivered - measurement.GasDelivered
            };
        }
    }
}
