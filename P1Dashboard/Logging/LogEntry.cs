﻿using Microsoft.Extensions.Logging;

namespace P1Dashboard.Logging
{
    public class LogEntry
    {
        public LogEntry(string categoryName, LogLevel logLevel, EventId eventId, string message)
        {
            CategoryName = categoryName;
            LogLevel = logLevel;
            EventId = eventId;
            Message = message;
        }

        public string CategoryName { get; }

        public LogLevel LogLevel { get; }

        public EventId EventId { get; }

        public string Message { get; }

        public string BackgroundClass
        {
            get
            {
                switch (LogLevel)
                {
                    case LogLevel.Critical:
                    case LogLevel.Error:
                        return "bg-danger";

                    case LogLevel.Debug:
                    case LogLevel.Trace:
                        return "bg-secondary";

                    case LogLevel.Information:
                        return "bg-info";

                    case LogLevel.Warning:
                        return "bg-warning";

                    default:
                        return string.Empty;
                }
            }
        }
    }
}
