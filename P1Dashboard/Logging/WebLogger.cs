﻿using System;
using Microsoft.Extensions.Logging;

namespace P1Dashboard.Logging
{
    public class WebLogger : ILogger
    {
        private readonly string _categoryName;
        private readonly WebLoggerProvider _webLoggerProvider;

        public WebLogger(string categoryName, WebLoggerProvider webLoggerProvider)
        {
            _categoryName = categoryName;
            _webLoggerProvider = webLoggerProvider;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var logEntry = new LogEntry(_categoryName, logLevel, eventId, formatter(state, exception));

            _webLoggerProvider.AddLogEntry(logEntry);
        }

        public bool IsEnabled(LogLevel logLevel) =>
            true;

        public IDisposable BeginScope<TState>(TState state) =>
            null;
    }
}
