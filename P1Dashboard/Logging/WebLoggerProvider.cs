﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace P1Dashboard.Logging
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class WebLoggerProvider : ILoggerProvider
    {
        private const int MaxQueueSize = 100;

        private readonly Queue<LogEntry> _logEntries = new Queue<LogEntry>();
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        private readonly Dictionary<string, ILogger> _loggerByName = new Dictionary<string, ILogger>();

        public IEnumerable<LogEntry> LogEntries
        {
            get
            {
                _lock.EnterReadLock();

                try
                {
                    return _logEntries.ToArray();
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public ILogger CreateLogger(string categoryName)
        {
            if (!_loggerByName.ContainsKey(categoryName))
                _loggerByName.Add(categoryName, new WebLogger(categoryName, this));

            return _loggerByName[categoryName];
        }

        public void AddLogEntry(LogEntry logEntry)
        {
            _lock.EnterWriteLock();

            try
            {
                _logEntries.Enqueue(logEntry);

                while (_logEntries.Count > MaxQueueSize)
                    _logEntries.Dequeue();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void Dispose()
        {
        }
    }
}
