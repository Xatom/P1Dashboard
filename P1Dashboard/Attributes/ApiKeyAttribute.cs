using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace P1Dashboard.Attributes
{
    public class ApiKeyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var settings = context.HttpContext.RequestServices.GetService<Settings>();
            var header = context.HttpContext.Request.Headers["Authorization"];
            var validHeader = header.Any() && header.Last() == $"Bearer {settings.ApiKey}";

            if (string.IsNullOrWhiteSpace(settings.ApiKey) || !validHeader)
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
