﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace P1Dashboard
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}
