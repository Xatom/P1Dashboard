﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using P1Dashboard.P1.Handlers;

namespace P1Dashboard.Hubs
{
    [Authorize]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class P1Hub : Hub
    {
        public P1Hub(CurrentInfoHandler currentInfoHandler)
        {
            _currentInfoHandler = currentInfoHandler;
        }

        public const string InfoCommand = "Info";

        private readonly CurrentInfoHandler _currentInfoHandler;

        public override async Task OnConnectedAsync()
        {
            await Clients.Caller.SendAsync(InfoCommand, _currentInfoHandler.Info);
        }
    }
}
