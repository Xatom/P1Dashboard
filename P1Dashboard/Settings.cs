﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace P1Dashboard
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class Settings
    {
        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public string Username { get; set; } = "user";

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public string Password { get; set; } = "pass";

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string ApiKey { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string ProxyServer { get; set; }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public string DatabasePath { get; set; } = "P1Dashboard.sqlite";

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public string ServerHost { get; set; } = "localhost";

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public int ServerPort { get; set; } = 8888;

        public TimeSpan ReconnectInterval { get; } = TimeSpan.FromSeconds(30);

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public int UpdateInterval { get; set; } = 2;

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public string SentryDsn { get; set; }

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
        public string Version { get; } = "Unknown Version";

        public string ConnectionString => $"Data Source={DatabasePath}";
    }
}
