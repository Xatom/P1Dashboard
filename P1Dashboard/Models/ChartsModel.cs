﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using P1Dashboard.Data;

namespace P1Dashboard.Models
{
    public class ChartsModel
    {
        public DateTime TimeStamp { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public List<MeasurementModel> DayMeasurements { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public List<MeasurementModel> WeekMeasurements { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public List<MeasurementModel> MonthMeasurements { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public List<MeasurementModel> YearMeasurements { get; set; }
    }
}
