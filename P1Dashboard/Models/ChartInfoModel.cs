﻿namespace P1Dashboard.Models
{
    public class ChartInfoModel
    {
        public ChartInfoModel(string name, string idPrefix)
        {
            Name = name;
            IdPrefix = idPrefix;
        }

        public string Name { get; }

        public string IdPrefix { get; }
    }
}
