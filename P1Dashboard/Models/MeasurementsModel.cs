﻿using P1Dashboard.Data;

namespace P1Dashboard.Models
{
    public class MeasurementsModel
    {
        public MeasurementModel Day { get; set; }

        public MeasurementModel Week { get; set; }

        public MeasurementModel Month { get; set; }

        public MeasurementModel Year { get; set; }
    }
}
