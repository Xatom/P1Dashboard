﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace P1Dashboard.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Measurements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EnergyDeliveredTariff1 = table.Column<decimal>(nullable: false),
                    EnergyDeliveredTariff2 = table.Column<decimal>(nullable: false),
                    EnergyReturnedTariff1 = table.Column<decimal>(nullable: false),
                    EnergyReturnedTariff2 = table.Column<decimal>(nullable: false),
                    GasDelivered = table.Column<decimal>(nullable: false),
                    Period = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measurements", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Measurements_Period",
                table: "Measurements",
                column: "Period");

            migrationBuilder.CreateIndex(
                name: "IX_Measurements_Period_Date",
                table: "Measurements",
                columns: new[] { "Period", "Date" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Measurements");
        }
    }
}
