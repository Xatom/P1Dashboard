using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using P1Dashboard.Data;

namespace P1Dashboard.Converters
{
    public class MeasurementModelConverter : JsonConverter<MeasurementModel>
    {
        public override void WriteJson(JsonWriter writer, MeasurementModel value, JsonSerializer serializer)
        {
            var data = JObject.FromObject(value);

            switch (value.Period)
            {
                case MeasurementPeriod.Day:
                    data[nameof(value.Date)] = $"{value.Date:d MMM \\'yy}";
                    break;

                case MeasurementPeriod.Week:
                    data[nameof(value.Date)] = $"W{ISOWeek.GetWeekOfYear(value.Date)} {value.Date:\\'yy}";
                    break;

                case MeasurementPeriod.Month:
                    data[nameof(value.Date)] = $"{value.Date:MMM \\'yy}";
                    break;

                case MeasurementPeriod.Year:
                    data[nameof(value.Date)] = $"{value.Date:yyyy}";
                    break;

                default:
                    throw new InvalidOperationException("Unknown period");
            }

            data.WriteTo(writer);
        }

        public override MeasurementModel ReadJson(JsonReader reader, Type objectType, MeasurementModel existingValue, bool hasExistingValue, JsonSerializer serializer) =>
            throw new NotImplementedException();
    }
}
