# P1 Dashboard
Simple dashboard that makes it possible to read P1 information over the internet using websockets.

This project was created for use with a Raspberry Pi 3 running Raspbian (`armv7l`), however it's possible to build Docker images for almost any OS and architecture yourself. It is intended for internal use; the application is not protected by a login.

# Usage
To start P1Dashboard, first start [serial-tcp](https://gitlab.com/Xatom/serial-tcp) and then:

```bash
docker pull registry.gitlab.com/xatom/p1dashboard/$ARCH:latest
docker stop p1dashboard
docker rm p1dashboard
docker run -d --name p1dashboard --restart always -p 80:80 \
    -e ServerHost=serial-tcp \
    -v /path/to/keys:/root/.aspnet/DataProtection-Keys:rw \
    -v /path/to/db.sqlite:/app/P1Dashboard.sqlite:rw \
    --network=bridge \
    --link=serial-tcp \
    registry.gitlab.com/xatom/p1dashboard/$ARCH:latest
```

Make sure to:
* Replace `$ARCH` with the CPU architecture of the host (`amd64` or `arm`)

The default username/password is `user`/`pass`. Please change these values (see [Configuration](#configuration)) to increase security.

# Configuration
The following options are available. You can add them to the docker run command by adding an -e flag:

```bash
docker run ...
    -e SomeOption=SomeValue \
```

* `Username` (default: `user`)
    Username to use for logging in to the application.
* `Password` (default: `pass`)
    Password to use for logging in to the application.
* `ApiKey` (default: none)  
    API key to use for external services.
* `ProxyServer` (default: none)
    Host name or IP address of trusted proxy servers (`X-Forwarded-*` headers will be accepted from this host).
* `DatabasePath` (default: `/app/data/P1Dashboard.sqlite`)
    Path to the SQLite database to store persistent data.
* `ServerHost` (default: `localhost`)  
    Server host to connect with for serial reading
* `ServerPort` (default: `8888`)  
    Server port to connect with for serial reading
* `UpdateInterval` (default: `2`)  
    Every Nth P1 message is processed, lower value means more updates 
* `SentryDsn` (default: none)  
    Sentry DSN to use for external error logging.

# Statistics
When using statistics, the first day of the week will be based on the set locale. You can change this by adding the following to the docker run command:

```bash
docker run ...
    -e LANG=$LANG
```

Make sure to:
* Replace `$LANG` with the desired culture (for example `nl_NL`)

# Troubleshooting

To check if your configuration is applied successfully, you can add the `PrintSettings=true` arguments to the `docker run` command:

```
docker run ...
    registry.gitlab.com/xatom/p1dashboard/$ARCH:latest PrintSettings=true
```

Note that this may print security sensitive information to the console and other logging providers (like the log that is publicly accessible via the web interface). Only use this to check if configuration is applied successfully (preferably with dummy data) and don't leave the app running with this setting enabled.
