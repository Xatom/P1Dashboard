var gulp = require('gulp'),
    pump = require('pump'),
    uglifyEs = require('uglify-es'),
    uglifyComposer = require('gulp-uglify/composer');

var rename = require('gulp-rename'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    cleanCss = require('gulp-clean-css'),
    uglify = uglifyComposer(uglifyEs, console);

var handleResult = (error, done) => {
    if (error !== undefined)
        console.error(error);

    done();
}

gulp.task('less', (done) => {
    return pump([
        gulp.src([
            './P1Dashboard/Content/Styles/Main.less'
        ]),
        less(),
        gulp.dest('./P1Dashboard/Content/Styles')
    ], (error) => handleResult(error, done));
});

gulp.task('concat-css', (done) => {
    return pump([
        gulp.src([
            './node_modules/bootstrap/dist/css/bootstrap.css',
            './node_modules/font-awesome/css/font-awesome.css',
            './P1Dashboard/Content/Styles/Main.css'
        ]),
        concat('Styles.css'),
        gulp.dest('./P1Dashboard/wwwroot/Out/Styles')
    ], (error) => handleResult(error, done));
});

gulp.task('concat-js', (done) => {
    return pump([
        gulp.src([
            './node_modules/chart.js/dist/Chart.js',
            './node_modules/vue/dist/vue.min.js',
            './node_modules/jquery/dist/jquery.slim.js',
            './node_modules/popper.js/dist/umd/popper.js',
            './node_modules/bootstrap/dist/js/bootstrap.js',
            './node_modules/@aspnet/signalr/dist/browser/signalr.js',
            './P1Dashboard/Content/Scripts/**/*.js'
        ]),
        concat('Scripts.js'),
        gulp.dest('./P1Dashboard/wwwroot/Out/Scripts')
    ], (error) => handleResult(error, done));
});

gulp.task('minify-css', (done) => {
    return pump([
        gulp.src([
            './P1Dashboard/wwwroot/Out/Styles/Styles.css'
        ]),
        cleanCss(),
        rename({ suffix: '.min' }),
        gulp.dest('./P1Dashboard/wwwroot/Out/Styles')
    ], (error) => handleResult(error, done));
});

gulp.task('minify-js', (done) => {
    return pump([
        gulp.src([
            './P1Dashboard/wwwroot/Out/Scripts/Scripts.js'
        ]),
        uglify({
            output: {
                comments: /license/i
            }
        }),
        rename({ suffix: '.min' }),
        gulp.dest('./P1Dashboard/wwwroot/Out/Scripts')
    ], (error) => handleResult(error, done));
});

gulp.task('copy-fonts', (done) => {
    return pump([
        gulp.src([
            './node_modules/font-awesome/fonts/*'
        ]),
        gulp.dest('./P1Dashboard/wwwroot/Out/fonts')
    ], (error) => handleResult(error, done));
});

gulp.task('watch-less', () => {
    return gulp.watch('./P1Dashboard/Content/Styles/**/*.less',
        gulp.series('less', 'concat-css', 'minify-css'));
});

gulp.task('watch-js', () => {
    return gulp.watch('./P1Dashboard/Content/Scripts/**/*.js',
        gulp.series('concat-js', 'minify-js'));
});


gulp.task('build', gulp.parallel(
    gulp.series('less', 'concat-css', 'minify-css'),
    gulp.series('concat-js', 'minify-js'),
    'copy-fonts'
));

gulp.task('watch', gulp.parallel(
    'watch-less',
    'watch-js'
));

gulp.task('default', gulp.series(
    'build',
    'watch'
));
