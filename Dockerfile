FROM mcr.microsoft.com/dotnet/core/aspnet:3.0

ENV DatabasePath data/P1Dashboard.sqlite

WORKDIR /app
COPY /Build .

EXPOSE 80

ENTRYPOINT ["dotnet", "P1Dashboard.dll"]
